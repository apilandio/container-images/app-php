.DEFAULT_GOAL := help

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

##
##Build
##---------------------------------------------------------------------------

build-development-image:          ## --> Build development image
	 docker build -t app-php:dev . --no-cache --build-arg ENVIRONMENT=development --progress plain
.PHONY: build-development-image

build-production-image:           ## --> Build production image
	 docker build -t app-php:prd . --no-cache --build-arg ENVIRONMENT=production --progress plain
.PHONY: build-production-image

##---------------------------------------------------------------------------
##
