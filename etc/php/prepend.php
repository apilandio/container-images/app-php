<?php

//
// Sourcing environment variables
//

const CONTAINER_ENVIRONMENT_FILE = '/etc/container_environment.json';

if (file_exists(CONTAINER_ENVIRONMENT_FILE)) {
    $containerEnvironment = json_decode(
        file_get_contents(CONTAINER_ENVIRONMENT_FILE),
        true
    );

    if (is_array($containerEnvironment)) {
        foreach ($containerEnvironment as $key => $value) {
            putenv(
                sprintf(
                    '%s=%s',
                    $key, $value
                )
            );
        }
    }
}
