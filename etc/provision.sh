#!/usr/bin/env bash

#
# Provision
#

set -ex

#
# Production only
#

if [[ "$ENVIRONMENT" = 'production' ]]
then
    echo 'Running production specific tasks...':
fi;

#
# Development only
#

if [[ "$ENVIRONMENT" = 'development' ]]
then
    echo 'Running development specific tasks...':
    apt-get install -y --no-install-recommends \
        iproute2 \
        git \
        php7.4-xdebug

    echo "xdebug.client_host="`/sbin/ip route|awk '/default/ { print $3 }'` >> /etc/php/7.4/fpm/conf.d/92-xdebug.ini
    echo "xdebug.client_host="`/sbin/ip route|awk '/default/ { print $3 }'` >> /etc/php/7.4/cli/conf.d/92-xdebug.ini
fi;
