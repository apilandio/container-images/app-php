FROM phusion/baseimage:focal-1.0.0

WORKDIR /code

ARG ENVIRONMENT=production

ADD etc/provision.sh /etc/provision.sh
ADD etc/service/nginx /etc/service/nginx/run
ADD etc/service/php-fpm /etc/service/php-fpm/run
ADD etc/service/supervisor /etc/service/supervisor/run

RUN export DEBIAN_FRONTEND=noninteractive \
    && set -ex \
    && add-apt-repository ppa:ondrej/php -y \
    && apt-get update \
    && apt-get -o Dpkg::Options::="--force-confnew" upgrade -y \
    && apt-get install -y --no-install-recommends -o Dpkg::Options::="--force-confnew" \
      make \
      wget \
      nano \
      nginx \
      unzip \
      supervisor \
      php7.4-cli \
      php7.4-fpm \
      php7.4-json \
      php7.4-intl \
      php7.4-curl \
      php7.4-mysql \
      php7.4-opcache \
      php7.4-zip \
      php7.4-mbstring \
      php7.4-bcmath \
      php7.4-xml \
      php7.4-gd \
    && apt-get autoremove -y -f \
    && apt-get clean -y

RUN set -ex \
    && update-alternatives --set php /usr/bin/php7.4

RUN set -ex \
    && chmod a+x /etc/provision.sh \
    && chmod a+x /etc/service/nginx/run \
    && chmod a+x /etc/service/php-fpm/run \
    && chmod a+x /etc/service/supervisor/run \
    && rm -f /etc/nginx/conf.d/* \
    && rm -f /etc/nginx/sites-enabled/* \
    && rm -f /etc/php/7.4/fpm/pool.d/*

RUN set -ex \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \
    && php -v \
    && composer

COPY etc/nginx/nginx.conf /etc/nginx/
COPY etc/nginx/vhost.conf /etc/nginx/conf.d/
COPY etc/php/prepend.php /etc/php/prepend.php
COPY etc/php/fpm/www.$ENVIRONMENT.conf /etc/php/7.4/fpm/pool.d/www.conf
COPY etc/php/cli/php.base.ini /etc/php/7.4/cli/conf.d/90-base.ini
COPY etc/php/cli/php.$ENVIRONMENT.ini /etc/php/7.4/cli/conf.d/91-custom.ini
COPY etc/php/fpm/php.base.ini /etc/php/7.4/fpm/conf.d/90-base.ini
COPY etc/php/fpm/php.$ENVIRONMENT.ini /etc/php/7.4/fpm/conf.d/91-custom.ini
COPY public/index.php /code/public/

RUN export DEBIAN_FRONTEND=noninteractive \
    && set -ex \
    && /etc/provision.sh

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 80
CMD ["/sbin/my_init"]
